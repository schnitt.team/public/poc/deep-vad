import math
import os
import logging
import wave
import random
from typing import List, Callable, Dict, Tuple
import librosa


import librosa.feature
import scipy.signal
from keras.utils import Sequence
import numpy as np

from schnitt.asr.trs import TrsParser, TrsLine

LOG = logging.getLogger(__name__)


class AudioData:
    def __init__(self, rec_id: str, audio_file: str, ch: int, nframe: int, hz: int, shift: int, win: int, dim: int, intervals: List[Tuple[float, float]],
                 lazy_load_x: bool = True, lazy_load_y: bool = True, exclude_ids: List[str] = None):
        """
        :param wav:
        :param nframe:
        :param intervals:
        """
        self.rec_id = rec_id
        self.fpath_wav = audio_file
        self.ch = ch
        self.hz = hz
        self.shift = shift
        self.win = win
        self.dim = dim
        self.intervals = intervals[:]
        self.nframe = nframe
        self.y = None
        self.x = None
        if not lazy_load_y:
            self.gen_y()
        if not lazy_load_x:
            self.gen_x()
        self.fh = None
        self.off = 0  # sample offset - incremented by 1

    def gen_x(self):
        if self.x is None:
            self.fh = wave.open(self.fpath_wav)
            self.x = np.frombuffer(self.fh.readframes(self.fh.getnframes()), dtype=np.int16) / 2**15
            self.fh.close()
            nch = self.fh.getnchannels()
            if nch > 1:
                self.x = self.x[(self.ch-1)::nch]
                # import matplotlib.pyplot as plt
                # plt.figure(0)
                # plt.plot(self.x[:16000*10])
                # plt.show()
                # plt.figure(1)
                # self.x = self.x[(self.ch-1)::nch]
                # plt.plot(self.x[:16000*10])
                # plt.show()
                # print("Fas")

    def clear(self):
        self.y = None
        self.x = None

    def gen_y(self) -> None:
        if self.y is not None:
            return
        self.y = np.zeros((self.nframe,))

        for start, end in self.intervals:
            """    s0       s1
             -------|********|--------
             --|   |------------------  t1 < s0
             -----|   |---------------  t0 < s0 < t1
             ----------|   |----------  s0 < t0 < t1 < s1 
             -------------|   |-------  
             -------------------|   |-  s1 < t0
            """
            s0 = int(start * self.hz)  # start in sample
            s1 = int(end * self.hz)  # end in sample
            i = math.floor(max(0, s0 - self.win) / self.shift)
            t0 = i * self.shift
            while i < self.y.shape[0]:
                t1 = t0 + self.win
                if t1 <= s0:
                    pass
                elif s1 < t0:
                    break
                elif t0 <= s0 < t1:
                    self.y[i] += (min(t1, s1) - s0) / self.win
                elif s0 <= t0:
                    self.y[i] += (min(t1, s1) - t0) / self.win
                else:
                    print("Faszom")
                t0 += self.shift
                i += 1

    def get_y(self, i, j, arr, off: int) -> int:
        if self.y is None:
            self.gen_y()
        arr[off:off+j-i] = self.y[i:j]
        return j-i

    def get_x(self, i, j, mat, off: int):
        if self.x is None:
            self.gen_x()
        ndone = 0
        for ix in range(i, j):
            t0 = ix * self.shift
            t1 = t0 + self.win
            # MFCC
            mfccs = librosa.feature.mfcc(y=self.x[t0:t1], sr=self.hz, n_fft=512, center=True, window="hann", n_mels=26)
            mat[off + ndone, :] = mfccs[1:13, 0]
            # mfccs = mfccs.flatten()
            # mfccs = mfccs.tolist()


            # freq, pow = scipy.signal.welch(self.x[t0:t1], fs=self.hz, nperseg=512, return_onesided=True)
            # mat[off + ndone, :] = pow[:self.dim]
            ndone += 1
        return j-i


class RawAudioLoader(Sequence):
    """
    On-the-fly data loading for ML training.
    Keeps label info in memory.
    """
    def __init__(self, hz: float, win: float, shift: float, dim: int, batch_size: int = 10_000, source_cnt: int = 10, log = None):
        """
        :param trs_files:
        :param source_cnt: simultaneously sampled audio files
        :param win: window size in seconds (e.g, 0.2 => 200ms)
        :param shift:  shift size in seconds (e.g. 0.001 => 10ms)
        """
        if batch_size % source_cnt != 0:
            raise ValueError(f"Batch size must be a multiple of source size={source_cnt}")
        self.batch_size = batch_size
        self.source_size = source_cnt
        self.per_source_batch_size = int(batch_size / source_cnt)

        self.hz = abs(float(hz))
        self.win: int = round(self.hz * abs(float(win)))
        self.shift: int = round(self.hz * abs(float(shift)))
        self.data: Dict[str, AudioData] = {}  # rec-id :: AudioData
        self.dim = dim

        self.tot_frame = 0
        self.batch_ix = 0
        self.batch_ix_max = 0
        self.log = log if log is not None else logging.getLogger(__name__)

    def get_rec_ids(self):
        return sorted(self.data.keys())

    def _add_audio(self, rec_id: str, fpath_audio: str, labels: List) -> None:
        wav = wave.open(fpath_audio)
        if wav.getframerate() != int(self.hz):
            LOG.error(f"Mismatch in sampling rate {wav.getframerate()} != {self.hz}. Skipping {fpath_audio}")
            return
        n = wav.getnframes()
        wav.close()
        nframe = int((n - self.win) / self.shift) + 1
        # TODO: remove
        assert n - (nframe * self.shift + self.win) < self.win

        # separate channels
        chs = {lab[0] for lab in labels}
        for ch in chs:
            key = rec_id, ch
            data = AudioData(rec_id=rec_id, ch=ch, audio_file=fpath_audio, nframe=nframe, hz=self.hz, shift=self.shift, win=self.win, dim=self.dim,
                  intervals=[(lab[1], lab[2]) for lab in labels if lab[0] == ch and lab[3] == 1], lazy_load_x=True, lazy_load_y=True)
            self.data[key] = data
            self.tot_frame += data.nframe

    def add_data(self, trs_files: List[str], trs2vad: Callable[[str], int], recid2path: Callable[[str], str],
                 max_audio: int = -1, exclude_ids: List[str] = None):
        for f in trs_files:
            if not os.path.isfile(f):
                raise ValueError(f"Non-existing file specified as input trs: {f}")
        parser = TrsParser()
        span: TrsLine

        intervals = []
        prev_rec_id = None
        for fpath in trs_files:
            for span in parser.parse(fpath):
                if span.rec_id in self.data or (exclude_ids is not None and span.rec_id in exclude_ids):
                    continue
                if span.rec_id != prev_rec_id:
                    if len(intervals) > 0:
                        LOG.debug(f"Parsing {len(self.data)+1:5d}: {prev_rec_id} (tot-frames:{self.tot_frame:,})")
                        fpath_audio = recid2path(prev_rec_id)
                        # if "A01M0096" == prev_rec_id:
                        self._add_audio(prev_rec_id, fpath_audio, intervals)
                        intervals.clear()
                        if max_audio > 0 and len(self.data) >= max_audio:
                            break
                    prev_rec_id = span.rec_id
                intervals.append((span.ch, span.start, span.end, trs2vad(span.label)))

        if len(intervals) > 0:
            fpath_audio = recid2path(prev_rec_id)
            self._add_audio(prev_rec_id, fpath_audio, intervals)

        self.log.info(f"Recording count: {len(self.data):12,d}")
        self.log.info(f"Frame     count: {self.tot_frame:12,d}")

    def init(self):
        data_list = sorted([(data.nframe, rec_id, ch, data) for (rec_id, ch), data in self.data.items()], reverse=True)

        self.sources = [[] for _ in range(self.source_size)]
        source_sizes = [0] * self.source_size
        # 1. add first item to each source list
        for i, (cnt, _, _, data) in enumerate(data_list[0:self.source_size]):
            ix = i % self.source_size
            self.sources[ix].append(data)
            source_sizes[ix] = data.nframe

        # 2. add remaining items to each source list
        for cnt, _, _, data in data_list[self.source_size:]:
            # get index of min size
            ix = min(range(len(source_sizes)), key=source_sizes.__getitem__)
            self.sources[ix].append(data)
            source_sizes[ix] += cnt

        # how many full batches are yielded
        batch_cnts = [math.floor(sz/self.per_source_batch_size) for sz in source_sizes]
        self.batch_ix_max = min(batch_cnts) - 1

        # random shuffle within source lists
        for data_lst in self.sources:
            random.shuffle(data_lst)

    def close(self):
        for data in self.data.values():
            data.clear()

    def __getitem__(self, ix: int):
        # batch_y = np.zeros(self.batch_size)
        batch_y = np.zeros(self.batch_size)
        batch_x = np.zeros((self.batch_size, self.dim))
        #batch_x = np.random.rand(self.batch_size, self.dim)
        batch_off = 0  # offset in target data

        frame_start = ix * self.per_source_batch_size
        frame_end = frame_start + self.per_source_batch_size
        # print(f"GETITIEM={ix}/{self.batch_ix_max}  {frame_start}-{frame_end}")
        for src_ix in range(self.source_size):
            off = 0  # per source group offset
            written = 0  # written sample per group
            for data in self.sources[src_ix]:
                if off + data.nframe < frame_start:
                    off += data.nframe
                    continue
                if off < frame_end and off + data.nframe >= frame_start:
                    f0 = max(off,           frame_start)
                    f1 = min(off+data.nframe, frame_end)
                    ncopied1 = data.get_y(f0-off, f1-off, batch_y, batch_off)
                    ncopied2 = data.get_x(f0-off, f1-off, batch_x, batch_off)
                    # import matplotlib.pyplot as plt
                    # plt.plot(batch_x[5])
                    # plt.show()
                    assert ncopied1 == ncopied2
                    batch_off += ncopied1
                    off += data.nframe
                    written += ncopied1
                if written >= self.per_source_batch_size:
                    break  # next source line

        # threshold at 0.1 -> above 10% the frame must be set to TRUE
        batch_y = np.clip(batch_y, 0, 1)
        batch_y[batch_y >= 0.1] = 1
        batch_y[batch_y < 0.1] = 0
        # 1-hot
        # batch_y = np.eye(2)[np.round(batch_y).astype("int")]
        return batch_x, batch_y

    def on_epoch_end(self):
        for data_lst in self.sources:
            random.shuffle(data_lst)

    def __len__(self):
        return self.batch_ix_max + 1

