# Development setup



# Requirements 

```bash
# install virtual environment
sudo apt-get install python3-venv

# Create virtual environment
python -m venv venv

# activate virtual environment
source venv/source/bin/activate

# install dependencies
pip install -r requirements
```
