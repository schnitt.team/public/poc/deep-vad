import os
import re
import logging

import keras
from keras import models, layers
import numpy as np
import tensorflow as tf

from schnitt.utils.conf import get_conf, get_resource_obj
from schnitt.vad.data_loader import RawAudioLoader

LOG = logging.getLogger(__name__)
SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
SCRIPT_NAME = os.path.splitext(os.path.basename(__file__))[0]
PRJ_DIR = os.path.normpath(os.path.join(SCRIPT_DIR, "..", "..", ".."))
DATA_DIR = os.path.join(PRJ_DIR, "data")
BLD_DIR = os.path.join(PRJ_DIR, "build", SCRIPT_NAME)


def get_checkpoint(mdl_dir: str) -> keras.callbacks.ModelCheckpoint:
    return keras.callbacks.ModelCheckpoint(
        filepath=os.path.join(mdl_dir, "e{epoch:03d}-{loss:.3f}.keras"),
        verbose=1, monitor="loss", mode="max", save_best_only=False
    )


class CsjData:
    def __init__(self):
        self.fpath_trs =  os.path.join(DATA_DIR, "csj-core.trs")
        conf_objs = get_conf("data.json")
        audio_conf = get_resource_obj(conf_objs, corpus="CSJ", variant="core", type="audio")
        if len(audio_conf) < 1:
            raise ValueError("No 'CSJ-core' section found in 'data.json'")
        self.audio_root = audio_conf[0]["path"]

    def rec_id2path(self, rec_id: str):
        rid = rec_id.upper()
        # return os.path.join(self.audio_root, rid[:3], f"{rid}.wav")
        return os.path.join(self.audio_root, f"{rid}.wav")

    def trs2vad(self, label: str) -> int:
        """
        Converts trs to label
        :param label:
        :return: 1 if speech 0 if not speech
        """
        clean = re.sub(r"<[^>]*?>", " ", label).strip()
        return 0 if clean == 0 else 1


def test_data() -> None:
    csj = CsjData()
    assert os.path.isfile(csj.fpath_trs)

    batch_size = 1000
    nsource = 1
    feat_dim = 12
    data_loader = RawAudioLoader(hz=16000, win=0.025, shift=0.01,
                                 dim=feat_dim,
                                 batch_size=batch_size, source_cnt=nsource)
    data_loader.add_data(trs_files=[csj.fpath_trs], max_audio=1,
                         trs2vad=csj.trs2vad, recid2path=csj.rec_id2path)

    data_loader.init()
    rec_ids = data_loader.get_rec_ids()
    print(rec_ids)
    x, y  = data_loader[0]
    print(rec_ids, x.shape)
    # for i in range(100):
    #     x, y = data_loader[i]
    import matplotlib.pyplot as plt
    # ax1 = plt.subplot(200)
    # ax2 = plt.subplot(300)
    # plt.imshow(np.transpose(x)) # , interpolation='none')
    x -= np.min(x)
    x /= np.sum(x)
    # norm = np.linalg.norm(x)
    plt.imshow(np.transpose(x), origin='lower', interpolation='none')
    plt.show()


def get_validation_data(dim: int, hz, win, shift, ix: int = 0):
    csj = CsjData()
    batch_size = 20000  # first 20sec from 10 audio
    val_loader = RawAudioLoader(hz=hz, win=win, shift=shift,
                                 dim=dim,
                                 batch_size=batch_size, source_cnt=10)
    val_loader.add_data(trs_files=[csj.fpath_trs], max_audio=10,
                         trs2vad=csj.trs2vad, recid2path=csj.rec_id2path)
    val_loader.init()
    # val_X = np.zeros((batch_size, dim))
    # val_Y = np.zeros((batch_size, 2))
    x, y = val_loader[ix]
    # off = 0
    # for i in range(nbatch):
    #     print(i)
    #     print(y.shape, x.shape)
    #     val_Y[off:off + y.shape[0],:] = y
    #     val_X[off:off + x.shape[0],:] = x
    #     off += y.shape[0]
    val_loader.close()
    rec_ids = val_loader.get_rec_ids()
    return x, y, rec_ids



class MinMaxPredEarlyStopper(keras.callbacks.Callback):
    def __init__(self, x, y):
        """
        :param x: input data
        :param y: prediction labels
        """
        super().__init__()
        self.x = x
        self.y = y

    def on_epoch_end(self, epoch, logs=None):
        pred_y = self.model.predict(self.x)
        diff = np.abs(max(pred_y)[0] - min(pred_y)[0])
        LOG.info("Validation output min-max: ({:.3f}, {:.3f}) (diff={:.3})".format(min(pred_y)[0], max(pred_y)[0], diff))
        if diff < 0.01:
            LOG.warning(f"Early stopping due to saturated output: diff={diff}")
            self.model.stop_training = True


def train20231218() -> None:
    csj = CsjData()
    assert os.path.isfile(csj.fpath_trs)

    tf.config.threading.set_intra_op_parallelism_threads(10)
    # tf.debugging.set_log_device_placement(True)
    # config = tf.compat.v1.ConfigProto(device_count={"CPU": 10, "GPU": 1})
    # keras.backend.tensorflow_backend.set_session(tf.Session(config=config))

    mdl_dir = os.path.join(BLD_DIR, "20231218")
    os.makedirs(mdl_dir, exist_ok=True)

    batch_size = 10_000
    nsource = 10
    input_dim = 12
    hz=16000
    win=0.025
    shift=0.02

    LOG.info(tf.config.list_physical_devices('GPU'))
    with tf.device("/GPU:0"):
        activation = "sigmoid"
        activation = "relu"
        activation = "leaky_relu"
        mdl = models.Sequential([
            # layers.BatchNormalization(input_dim=input_dim, axis=1),
            layers.Dense(256, activation=activation, input_dim=input_dim),
            layers.Dense(128, activation=activation),
            layers.Dense(64, activation=activation),
            layers.Dense(32, activation=activation),
            layers.Dense(1, activation="sigmoid")
        ])

        opt = keras.optimizers.SGD(learning_rate=0.01)
        mdl.compile(loss='binary_crossentropy', optimizer=opt, metrics=["accuracy"])

        # opt = keras.optimizers.RMSprop(learning_rate=0.01)
        # opt = keras.optimizers.Adam(learning_rate=0.001)
        # mdl.compile(loss='mean_squared_error', optimizer=opt)
        # mdl.build(input_shape=(input_dim,))
        # print(mdl.summary())

        val_x, val_y, val_ids = get_validation_data(input_dim, hz=hz, win=win, shift=shift)

        data_loader = RawAudioLoader(hz=hz, win=win, shift=shift, dim=input_dim,# win=0.1, shift=0.01, dim=input_dim,
                                     batch_size=batch_size, source_cnt=nsource)
        data_loader.add_data(trs_files=[csj.fpath_trs], max_audio=100, exclude_ids=val_ids,
                             trs2vad=csj.trs2vad, recid2path=csj.rec_id2path)
        data_loader.init()

        tensorboard_callback = keras.callbacks.TensorBoard(log_dir=f"{mdl_dir}/logs", update_freq=1)
        early_stopper = MinMaxPredEarlyStopper(val_x, val_y)


        mdl.fit(data_loader, batch_size=batch_size, epochs=20, verbose=1,
                validation_data=(val_x, val_y),
                callbacks=[get_checkpoint(mdl_dir), tensorboard_callback, early_stopper])


def infer():
    mdl_path = "/home/kinoko/GIT/schnitt.team/poc/deep-vad/build/train_sinc/20231218/e020-0.302.keras"

    input_dim = 12
    hz=16000
    win=0.025
    shift=0.02

    import tensorflow as tf
    mdl = tf.keras.models.load_model(mdl_path)

    val_x, val_y, val_ids = get_validation_data(input_dim, hz=hz, win=win, shift=shift)
    print(val_ids)
    pred_y = mdl.predict(val_x)
    print(pred_y)




def train20231219():
    csj = CsjData()
    assert os.path.isfile(csj.fpath_trs)



if __name__ == '__main__':
    logging.basicConfig(format="%(asctime)s [%(levelname)s] %(module)s.%(funcName)s %(message)s", level=logging.DEBUG)
    logging.getLogger('matplotlib.font_manager').setLevel(logging.ERROR)
    # test_data()
    # train20231218()
    infer()

