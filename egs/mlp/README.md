# Multi Layer Perceptrion VAD
* this is the most basic 

``` 
_________________________________________________________________
 Layer (type)                Output Shape              Param #   
=================================================================
 batch_normalization (Batch  (None, 200)               800       
                      Normalization)                                                                
 dense (Dense)               (None, 200)               40200                                                                      
 dense_1 (Dense)             (None, 128)               25728                                                                      
 dense_2 (Dense)             (None, 128)               16512                                                                      
 dense_3 (Dense)             (None, 128)               16512                                                                      
 dense_4 (Dense)             (None, 2)                 258       
                                                                 
=================================================================
Total params: 100010 (390.66 KB)
Trainable params: 99610 (389.10 KB)
Non-trainable params: 400 (1.56 KB)
_________________________________________________________________
```