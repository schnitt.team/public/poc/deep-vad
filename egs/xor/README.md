# Hello World with Keras
* to test keras installation
* simplest task: logical gate **XOR**



| in | in | out | 
|----|----|-----|
| 1  | 1  | 0   | 
| 1  | 0  | 1   |
| 0  | 1  | 1   |
| 0  | 0  | 0   |


![xor.png](doc%2Fxor.png)

```
_________________________________________________________________
 Layer (type)                Output Shape              Param #   
=================================================================
 dense (Dense)               (2, 4)                    12        
 dense_1 (Dense)             (2, 1)                    5         
=================================================================
Total params: 17 (68.00 Byte)
Trainable params: 17 (68.00 Byte)
Non-trainable params: 0 (0.00 Byte)
_________________________________________________________________

```