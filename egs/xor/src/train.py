from keras import models, layers
from keras.optimizers import SGD
import numpy as np
from keras.utils import plot_model

X = np.array([[0,0],[0,1],[1,0],[1,1]])
y = np.array([[0],[1],[1],[0]])


mdl = models.Sequential([
    layers.Dense(4, activation="tanh"),
    layers.Dense(1, activation="sigmoid")
])

opt = SGD(learning_rate=0.1)
mdl.compile(loss='mean_squared_error', optimizer=opt)


# mdl.build(input_shape=(2,))
mdl.fit(X, y, batch_size=1, epochs=1000, verbose=1)
#print(mdl.summary())
#plot_model(mdl, to_file="../doc/xor.png", show_shapes=True, show_layer_names=False)


print(mdl.summary())